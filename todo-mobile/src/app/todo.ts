export class Todo{
    _id:number;
    title:string;
    isCompleted:boolean;
}