/**
 * @description Import Global Modules
 */
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * @description Import Local Services
 */
import { TodoService } from '../../services/todo.service';

/**
 * @description Import Local Pages
 */
import { TodoDetailsPage } from '../todo-details/todo-details';

/**
 * @description Import Todo Model
 */
import { Todo } from '../../app/todo';

@Component({
  selector: 'page-todo',
  templateUrl: 'todos.html',
  providers:[TodoService]
})
export class TodosPage implements OnInit {
   
  /** 
    * @type {any}
  */
   todos: Todo[];

  constructor(public navCtrl: NavController, private navParams:NavParams, private todoService: TodoService) {
        
  }
  
  /** 
    * @description onInit subscribe todo list
  */
  ngOnInit(){
     this.todoService.getTodos().subscribe(todos => {
         this.todos = todos;
         console.log(this.todos);
     })
  }
  
  /** 
    * @description onClick redirect to todo details page
  */
  todoSelected(event, todo){
    this.navCtrl.push(TodoDetailsPage, {
      todo:todo
    });
  }

  
  /** 
    * @description onClick redirect to todo details page
    * @param event
    * @param {Object}
  */
  updateStatus(todo:any){
    
      var _todo = {
        _id: todo._id,
        text: todo.text,
        isCompleted: !todo.isCompleted
      };
      this.todoService.updateTodo(_todo)
      .subscribe(data => {
        todo.isCompleted = !todo.isCompleted;
      });
    }
}