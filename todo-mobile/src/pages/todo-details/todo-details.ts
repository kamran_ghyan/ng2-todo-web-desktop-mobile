/**
 * @description Import Global Modules
 */
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * @description Import local services
 */
import { TodoService } from '../../services/todo.service';

/**
 * @description Import Local Pages
 */
import { TodosPage } from '../todos/todos';

/**
 * @description Import Todo Model
 */
import { Todo } from '../../app/todo';

@Component({
  selector: 'page-todo-details',
  templateUrl: 'todo-details.html',
  providers:[TodoService]
})

export class TodoDetailsPage {

  /** 
    * @type {Object.<any>}
  */
  todo: Todo[];

  /** 
    * @type {Object.<any>}
  */
  todos:any;
  
  /** 
    * @type {Object.<any>}
  */
  result:any;

  constructor(public navCtrl: NavController, private navParams:NavParams, private todoService: TodoService) {
    this.todo = this.navParams.get("todo");

     this.todoService.getTodos().subscribe(todos => {
         this.todos = todos;
     })
  }

  /**
   * @description Delete todo
   * @param {number} id of todo for deletion
  */
  deleteTodo(id:number){

    this.todoService.deleteTodo(id).subscribe(data => {
        this.result = data;
    },
    err => console.log(err),
    () => console.log('Todo Delete.....'));

    this.navCtrl.setRoot(TodosPage);

  }

  /** 
    * @description onClick redirect to todo details page
    * @param event
    * @param {Object}
  */
  updateStatus(todo:any){
    
      var _todo = {
        _id: todo._id,
        text: todo.text,
        isCompleted: !todo.isCompleted
      };
      this.todoService.updateTodo(_todo)
      .subscribe(data => {
        todo.isCompleted = !todo.isCompleted;
      });
    }
  
}