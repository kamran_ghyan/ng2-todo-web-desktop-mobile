/**
 * @description Import Global Modules
 */
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * @description Import Local Services
 */
import { TodoService } from '../../services/todo.service';

/**
 * @description Import Local Pages
 */
import { TodosPage } from '../todos/todos';


declare var window: any;

@Component({
  selector: 'page-add-todo',
  templateUrl: 'add-todo.html',
  providers:[TodoService]
})
export class AddTodoPage {

  /** 
    * @type {string}
  */
  text:string;

  /** 
    * @type {Object<Any>}
  */
  todos:any;

  constructor(public navCtrl: NavController, private todoService: TodoService) {

    this.todoService.getTodos().subscribe(todos => {
         this.todos = todos;
     })

  }

 /**
  * @description Add Todo
 */
  onSubmit(){
    var newTodo = {
        text: this.text,
        isCompleted: false
      };
    // Save todo
    this.todoService.saveTodo(newTodo).subscribe(res => {
        this.text = '';
    },
    err => console.log(err),
    () => console.log('Todo Added.....'));

    this.navCtrl.setRoot(TodosPage);
  }

}