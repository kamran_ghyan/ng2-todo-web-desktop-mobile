import { Component }         from '@angular/core';

// Local imports
import { TodoService } from './services/todo.service';

@Component({
    selector: 'my-app',
    styleUrls: [
        'app/assets/css/angular-material.css'
    ],
    templateUrl: 'app/app.component.html',
    providers:[TodoService]
})
export class AppComponent { }
