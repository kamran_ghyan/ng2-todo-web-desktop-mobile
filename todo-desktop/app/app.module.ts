import { NgModule }      	from '@angular/core';
import { BrowserModule } 	from '@angular/platform-browser';
import { FormsModule }   	from '@angular/forms';
import { MaterialModule }   from '@angular/material';
import { HttpModule }       from '@angular/http';

import { AppComponent }  	from './app.component';
import { TodosComponent }   from './components/todos.component';

import 'hammerjs';

@NgModule({
    imports:      [ 
    	BrowserModule,
        HttpModule,
    	FormsModule,
    	MaterialModule.forRoot()
    ],
    declarations: [ AppComponent, TodosComponent ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }
