/**
 * @description Import Global Modules
 */
import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';


export class TodoService{
    /**
     * @type any
    */
    http:any;

    /**
     * @type string
    */
    apiKey:string;

    /**
     * @type string
    */
    todosUrl:string;
   
    constructor(@Inject(Http) private _http: Http){
        this.http = _http;
        this.apiKey = 'lQum0P1UfwCj1ykGe5tm1whlQStEtQq1';
        this.todosUrl = 'https://api.mlab.com/api/1/databases/meantodos_kg/collections/todos?apiKey='+this.apiKey;
    }

    /**
     * @description get list of todo
    */
    getTodos(){
        return this.http.get(this.todosUrl)
          .map(res => res.json());
    }

    /**
     * @description Add todo
     * @param {object}
     * @returns {Object.<Any>}
    */
    saveTodo(todo){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.todosUrl, JSON.stringify(todo), {headers:headers})
        .map(res => res.json());
    }

    /**
     * @description Delete todo
     * @param {number} id of todo for deletion
    */
    deleteTodo(id:number){
        let todosUrl = 'https://api.mlab.com/api/1/databases/meantodos_kg/collections/todos/'+ id +'?apiKey='+this.apiKey;
        return this._http.delete(todosUrl)
        .map(res => res.json());
    }

    /**
     * @description Delete todo
     * @param {Object} for update task
    */
     updateTodo(todo:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let todosUrl = 'https://api.mlab.com/api/1/databases/meantodos_kg/collections/todos/'+ todo._id.$oid +'?apiKey='+this.apiKey;
        return this._http.put(todosUrl, JSON.stringify(todo), {headers:headers})
        .map(res => res.json());
    }

}