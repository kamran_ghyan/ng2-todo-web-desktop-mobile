// Global Imports
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { TodoService } from '../services/todo.service';

// Model Imports
import { Todo } from '../todo';

// Todo MetaData
@Component({
  moduleId: module.id,
  selector: 'todos',
  styleUrls: ['./todo.component.css'],
  templateUrl: './todos.component.html',
})

// Todo Class
export class TodosComponent implements OnInit  {
  
  todos: Todo[];

  constructor(private _todoService:TodoService){
  }

  ngOnInit(){
    this.todos = [];
    this._todoService.getTodos()
    .subscribe(todos => {
      this.todos = todos;
    });
  }
  // Save todo
  addTodo(event:any, todoText:any){

      var newTodo = {
        _id: 1,
        text: todoText.value,
        isCompleted: false
      };

      this._todoService.saveTodo(newTodo).subscribe(res => {
        this.todos.push(newTodo);
        todoText.value = '';
    });
  }

  // Edit sate mode
    setEditState(todo:any, state:any){
        if(state){
            todo.isEditMode = state;
        } else {
            delete todo.isEditMode;
        }
    }

    // Update todo status
    updateStatus(todo:any){
      var _todo = {
        _id: todo._id,
        text: todo.text,
        isCompleted: !todo.isCompleted
      };
      this._todoService.updateTodo(_todo)
      .subscribe(data => {
        todo.isCompleted = !todo.isCompleted;
      });
    }

    // Update todo text
    updateTodoText(event:any, todo:any){
      if(event.which === 13){
        todo.text = event.target.value;
        var _todo = {
          _id: todo._id,
          text: todo.text,
          isCompleted: !todo.isCompleted
        };

        this._todoService.updateTodo(_todo)
        .subscribe(data => {
          this.setEditState(todo, false);
        });
      }
      
    }

    // Delete todo
    deleteTodo(todo:any){
      var todos = this.todos;

      this._todoService.deleteTodo(todo)
      .subscribe(data => {
        if(data.n == 1){
          for(var i = 0; i < todos.length; i++){
              if(todos[i]._id == todo._id){
                todos.splice(i,1);
              }
          }
        }
      });
    }

 } // End of class