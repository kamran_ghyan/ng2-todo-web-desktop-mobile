// Global Modules
var express     = require('express');
var path        = require('path');
var bodyParser  = require('body-parser');

// Routes
var index = require('./routes/index');
var todos = require('./routes/todos');

// set port
var port = 3000;

// Main app
var app = express();


// View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.use(express.static(path.join(__dirname, 'client')));

// Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/', index);
app.use('/api/v1/',todos);

app.listen(port, function(){
    console.log('Server start on port ' + port);
});